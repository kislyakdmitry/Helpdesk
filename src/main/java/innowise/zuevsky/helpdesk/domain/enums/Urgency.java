package innowise.zuevsky.helpdesk.domain.enums;

public enum Urgency {
    CRITICAL, HIGH, AVERAGE, LOW
}
